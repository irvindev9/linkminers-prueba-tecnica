<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="https://img.icons8.com/fluent-systems-regular/30/000000/empty-hourglass.png"/>
    <title>Linkminers Test</title>
    {{-- Styles --}}
    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    @stack('styles')
</head>
<body>
    @include('partials.nav')

    {{-- Content --}}
    <div id="app" class="container">
        @yield('content')
    </div>

    {{-- Scripts --}}
    <script src="{{mix('js/app.js')}}"></script>
    @stack('scripts')
</body>
</html>